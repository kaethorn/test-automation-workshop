module.exports = class Products {
  constructor () {
    this.productsPerZipCode = {
      8000: [{
        name    : 'Zurich Super Maxi 4000',
        category: 'primary'
      }, {
        name    : 'Zurich Standard 2000',
        category: 'secondary'
      }],
      4000: [{
        name    : 'Basel Mega Offer',
        category: 'primary'
      }]
    };
    this.productsPerAgeGroup = {
      JUNIOR: [{
        name    : 'Junior Fun Basic',
        category: 'secondary'
      }, {
        name    : 'Junior Fun Max',
        category: 'primary'
      }],
      STUDENT: [{
        name    : 'Student Offer',
        category: 'primary'
      }],
      SENIOR: [{
        name    : 'Senior Supreme',
        category: 'primary'
      }]
    };

    this.globalProducts = [{
      name    : 'Basic Excellence',
      category: 'success'
    }, {
      name    : 'Standard Joy',
      category: 'success'
    }, {
      name    : 'Complete Experience',
      category: 'success'
    }];
  }

  getAgeGroup (dob) {
    dob = dob.split('.');
    const ageYears = (Date.now() - new Date(dob[2], dob[1], dob[0])) / (1000 * 60 * 60 * 24 * 365.25);
    if (0 <= ageYears && ageYears <= 18) {
      return 'JUNIOR';
    } else if (ageYears <= 28) {
      return 'STUDENT';
    } else if (ageYears >= 67) {
      return 'SENIOR';
    }
  }

  match (zip, dob) {
    return this.globalProducts
      .concat(this.productsPerZipCode[zip])
      .concat(this.productsPerAgeGroup[this.getAgeGroup(dob)])
      .filter(item => !!item);
  }
};
