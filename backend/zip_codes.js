module.exports = class ZipCodes {
  constructor () {
    const fs = require('fs');
    const path = require('path');
    this.codes = String(fs.readFileSync(path.join(__dirname, 'pkv_PLZ.csv'), 'latin1')).split('\n')
      .reduce((result, code) => {
        const row = code.split(';');
        const zip = Number.parseInt(row[0]);
        if (zip) {
          result[zip] = row[5];
        }
        return result;
      }, {});
  }

  lookup (input) {
    return this.codes[input];
  }
};
