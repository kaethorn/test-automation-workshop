describe('zip codes', () => {

  beforeEach(function () {
    this.module = new (require('../zip_codes'))();
  });

  it('contains all Swiss zip codes', function () {
    expect(Object.keys(this.module.codes).length).toBe(3371);
  });

  describe('#lookup', () => {

    it('returns the city name for the given zip code', function () {
      expect(this.module.lookup('9000')).toEqual('St. Gallen');
    });
  });
});
