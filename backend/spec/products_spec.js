describe('products', () => {

  beforeEach(function () {
    // Proxy date to 2018.01.01.
    var baseTime = new Date(2018, 1, 1);
    jasmine.clock().mockDate(baseTime);

    this.module = new (require('../products'))();
  });

  it('populates product groups', function () {
    expect(this.module.productsPerZipCode).toBeDefined();
    expect(this.module.productsPerAgeGroup).toBeDefined();
    expect(this.module.globalProducts).toBeDefined();
  });

  describe('#getAgeGroup', () => {

    describe('for a person born 10 years ago', () => {

      it('returns the appropriate age group', function () {
        expect(this.module.getAgeGroup('01.01.2008')).toBe('JUNIOR');
      });
    });

    describe('for a person born 20 years ago', () => {

      it('returns the appropriate age group', function () {
        expect(this.module.getAgeGroup('01.01.1998')).toBe('STUDENT');
      });
    });

    describe('for a person born 70 years ago', () => {

      it('returns the appropriate age group', function () {
        expect(this.module.getAgeGroup('01.01.1948')).toBe('SENIOR');
      });
    });
  });

  describe('#match', () => {

    it('adds all global products', function () {
      this.module.globalProducts.forEach((globalProduct) => {
        expect(this.module.match(4000, '01.01.1998')).toContain(globalProduct);
      });
    });

    it('adds zip code specific products', function () {
      this.module.productsPerZipCode[4000].forEach((globalProduct) => {
        expect(this.module.match(4000, '01.01.1998')).toContain(globalProduct);
      });
    });

    it('adds age specific products', function () {
      this.module.productsPerAgeGroup.STUDENT.forEach((globalProduct) => {
        expect(this.module.match(4000, '01.01.1998')).toContain(globalProduct);
      });
    });
  });
});
