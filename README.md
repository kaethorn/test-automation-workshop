# Test Automation Sample project

Contains three components:

* An `api` server providing a JSON API.
* A `backend` providing business logic as a module.
* A `webapp` interface.

## Static code analysis

Uses [ESLint](https://eslint.org/) and [TSLint](https://palantir.github.io/tslint/), see [webapp/tslint.json](webapp/tslint.json) and [api/.eslintrc.json](api/.eslintrc.json).

```shell
cd api && npm run lint && cd ..
cd backend && npm run lint && cd ..
cd webapp && npm run lint && cd ..
```

## Unit tests

Uses [Jasmine](https://jasmine.github.io/), see [webapp/src/app/**/*.spec.ts](webapp/src/app/).

```shell
cd api && npm test && cd ..
cd backend && npm test && cd ..
cd webapp && npm run test -- --watch=false --progress=false && cd ..
```

## Code coverage

Uses [Istanbul](https://istanbul.js.org/) and [Jasmine](https://jasmine.github.io/).

```shell
cd backend && npm run coverage && cd ..
cd webapp && npm run test -- --watch=false --progress=false --code-coverage && cd ..
google-chrome-stable backend/coverage/lcov-report/index.html
google-chrome-stable webapp/coverage/index.html
```

## API tests

Uses [Jasmine](https://jasmine.github.io/), see [api/spec/server_spec.js](api/spec/server_spec.js).

## Complexity analysis

Uses [Plato](https://github.com/es-analysis/plato).

```shell
npm run complexity && cd plato && google-chrome-stable index.html
```

## Integration tests

Uses [Jasmine](https://jasmine.github.io/), see [spec/integration_spec.js](spec/integration_spec.js).

```shell
npm test -- --filter=Integration
```

## Load tests

Uses [Artillery](https://artillery.io/), see [spec/load_spec.js](spec/load_spec.js).

```shell
npm run start-server
npm run artillery
```

## GUI tests

Uses [Protractor](https://www.protractortest.org), see [webapp/e2e](webapp/e2e).

```shell
cd webapp && npm run e2e && cd ..
```
