const parse = require('lcov-parse');

const path = process.argv[2];
const expected = process.argv[3];
parse(path, (err, data) => {
  const found = data.reduce((result, file) => {
    return result + file.lines.found;
  }, 0);
  const hit = data.reduce((result, file) => {
    return result + file.lines.hit;
  }, 0);
  const coverage = Math.round((100 * hit / found) * 100) / 100;

  // eslint-disable-next-line no-console
  console.log(`Coverage ${ coverage }% (${ hit }/${ found }), expected ${ expected }%.`);

  if (coverage < expected) {
    process.exit(1);
  }
});
