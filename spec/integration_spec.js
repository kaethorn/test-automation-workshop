describe('Integration', () => {

  beforeEach(function () {
    this.api = require('../api/server');
  });

  it('has available components', function () {
    expect(this.api).toBeDefined();
  });
});
