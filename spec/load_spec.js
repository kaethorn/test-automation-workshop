const { fork, exec } = require('child_process');
const fs = require('fs');

describe('Load', () => {

  beforeEach(function (done) {
    this.server = fork('api/server', [ '--silent' ]);
    this.server.on('message', data => {
      if (data === 'Server started') {
        done();
      }
    });
  });

  beforeEach(function (done) {
    exec('npm run artillery', (error) => {
      expect(error).toBeFalsy();
      fs.readFile('artillery.json', (error, result) => {
        expect(error).toBeFalsy();
        this.result = JSON.parse(result);
        done();
      });
    });
  }, 20000);

  afterEach(function () {
    this.server.kill();
  });

  it('completes all requests', function () {
    expect(this.result.aggregate.scenariosCreated)
      .toEqual(this.result.aggregate.scenariosCompleted);

    expect(this.result.aggregate.requestsCompleted)
      .toEqual(this.result.aggregate.codes['200']);

    expect(this.result.aggregate.errors).toEqual({});
  });

  it('has acceptable latency', function () {
    expect(this.result.aggregate.latency.max).toBeLessThan(40);
    expect(this.result.aggregate.latency.p99).toBeLessThan(10);
  });
});
