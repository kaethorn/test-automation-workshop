import { ProductFormPage } from './product-form.po';

describe('Validation', () => {
  let page: ProductFormPage;

  beforeAll(() => {
    page = new ProductFormPage();
    page.navigateTo();
  });

  it('does not show any validation errors by default', () => {
    expect(page.errors.name.isDisplayed()).toBe(false);
    expect(page.errors.dob.isDisplayed()).toBe(false);
    expect(page.errors.zip.isDisplayed()).toBe(false);
  });

  it('requires a name', () => {
    page.inputs.name.sendKeys('W');
    page.inputs.name.sendKeys(protractor.Key.BACK_SPACE);
    expect(page.errors.name.isDisplayed()).toBe(true);
    expect(page.errors.name.getText()).toEqual('Name is required.');
  });

  it('requires a date of birth', () => {
    page.inputs.dob.sendKeys('2');
    page.inputs.dob.sendKeys(protractor.Key.BACK_SPACE);
    expect(page.errors.dob.isDisplayed()).toBe(true);
    expect(page.errors.dob.getText()).toEqual('Date of birth is required.');
  });

  it('requires a valid date of birth', () => {
    page.inputs.dob.sendKeys('1985.10.08');
    expect(page.errors.dob.isDisplayed()).toBe(true);
    expect(page.errors.dob.getText()).toEqual('Format must be DD.MM.YYYY.');
  });

  it('requires a zip code', () => {
    page.inputs.zip.sendKeys('8');
    page.inputs.zip.sendKeys(protractor.Key.BACK_SPACE);
    expect(page.errors.zip.isDisplayed()).toBe(true);
    expect(page.errors.zip.getText()).toEqual('Zip code is required.');
  });

  it('requires a valid zip code', () => {
    page.inputs.zip.sendKeys('A900');
    expect(page.errors.zip.isDisplayed()).toBe(true);
    expect(page.errors.zip.getText()).toEqual('Format must be four digits.');
    page.inputs.zip.clear().sendKeys('9000');
    expect(page.errors.zip.isDisplayed()).toBe(false);
    page.inputs.zip.clear().sendKeys('90000');
    expect(page.errors.zip.isDisplayed()).toBe(true);
    expect(page.errors.zip.getText()).toEqual('Format must be four digits.');
  });

  it('populates the city with a valid zip code', () => {
    page.inputs.zip.clear().sendKeys('8000');
    expect(page.city.isPresent()).toBe(true);
    expect(page.city.getAttribute('value')).toEqual('Zürich');
  });

  it('enables the submit button with valid input', () => {
    page.inputs.name.clear().sendKeys('Walther');
    page.inputs.dob.clear().sendKeys('08.10.1985');
    expect(page.submit.isEnabled()).toBe(true);
  });
});
