import { ProductFormPage } from './product-form.po';

describe('ProductForm component', () => {
  let page: ProductFormPage;

  beforeAll(() => {
    page = new ProductFormPage();
    page.navigateTo();
  });

  it('should display a title', () => {
    expect(page.title.getText()).toEqual('Product Finder ™');
  });

  it('should display all input fields', () => {
    expect(page.inputs.name.isPresent()).toBe(true);
    expect(page.inputs.dob.isPresent()).toBe(true);
    expect(page.inputs.zip.isPresent()).toBe(true);
  });

  it('does not show a city field by default', () => {
    expect(page.city.isPresent()).toBe(false);
  });

  it('should contain a disabled submit button', () => {
    expect(page.submit.isPresent()).toBe(true);
    expect(page.submit.isEnabled()).toBe(false);
  });
});
