import { ProductFormPage } from './product-form.po';

describe('Product search', () => {
  let page: ProductFormPage;

  beforeAll(() => {
    page = new ProductFormPage();
    page.navigateTo();
  });

  it('finds basic products', () => {
    expect(page.result.count()).toBe(0);
    page.inputs.name.sendKeys('Walther');
    page.inputs.dob.sendKeys('08.10.1985');
    page.inputs.zip.sendKeys('9000');
    expect(page.submit.isEnabled()).toBe(true);
    page.submit.click();
    expect(page.result.count()).toBe(3);
    expect(page.result.get(0).getText()).toEqual('Complete Experience');
    expect(page.result.get(1).getText()).toEqual('Standard Joy');
    expect(page.result.get(2).getText()).toEqual('Basic Excellence');
  });

  it('finds products depending on the zip code', () => {
    page.inputs.zip.clear().sendKeys('8000');
    page.submit.click();
    expect(page.result.count()).toBe(5);
    expect(page.result.get(0).getText()).toEqual('Zurich Super Maxi 4000');
    expect(page.result.get(1).getText()).toEqual('Zurich Standard 2000');
    expect(page.result.get(2).getText()).toEqual('Complete Experience');
    expect(page.result.get(3).getText()).toEqual('Standard Joy');
    expect(page.result.get(4).getText()).toEqual('Basic Excellence');
  });

  it('finds products for students', () => {
    page.inputs.zip.clear().sendKeys('9000');
    page.inputs.dob.clear().sendKeys('02.04.1999');
    page.submit.click();
    expect(page.result.count()).toBe(4);
    expect(page.result.get(0).getText()).toEqual('Student Offer');
    expect(page.result.get(1).getText()).toEqual('Complete Experience');
    expect(page.result.get(2).getText()).toEqual('Standard Joy');
    expect(page.result.get(3).getText()).toEqual('Basic Excellence');
  });

  it('finds products for juniors', () => {
    page.inputs.dob.clear().sendKeys('02.04.2005');
    page.submit.click();
    expect(page.result.count()).toBe(5);
    expect(page.result.get(0).getText()).toEqual('Junior Fun Max');
    expect(page.result.get(1).getText()).toEqual('Junior Fun Basic');
    expect(page.result.get(2).getText()).toEqual('Complete Experience');
    expect(page.result.get(3).getText()).toEqual('Standard Joy');
    expect(page.result.get(4).getText()).toEqual('Basic Excellence');
  });

  it('finds products for seniors', () => {
    page.inputs.dob.clear().sendKeys('02.04.1930');
    page.submit.click();
    expect(page.result.count()).toBe(4);
    expect(page.result.get(0).getText()).toEqual('Senior Supreme');
    expect(page.result.get(1).getText()).toEqual('Complete Experience');
    expect(page.result.get(2).getText()).toEqual('Standard Joy');
    expect(page.result.get(3).getText()).toEqual('Basic Excellence');
  });
});
