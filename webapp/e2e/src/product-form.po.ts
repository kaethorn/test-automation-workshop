import { browser, by, element } from 'protractor';

export class ProductFormPage {
  title = element(by.css('h1'));

  inputs = {
    name: element(by.name('name')),
    dob : element(by.name('dob')),
    zip : element(by.name('zip'))
  };

  errors = {
    name: element(by.css('[name="name"] + .alert')),
    dob : element(by.css('[name="dob"] + .alert')),
    zip : element(by.css('[name="zip"] + .alert')),
  };

  city = element(by.name('city'));

  submit = element(by.buttonText('Find products'));

  result = element.all(by.css('.products .alert'));

  navigateTo() {
    return browser.get('/');
  }
}
