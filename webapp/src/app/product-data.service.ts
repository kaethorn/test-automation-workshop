import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { City } from './city';
import { PersonalData } from './personal-data';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductDataService {

  constructor(
    private http: HttpClient
  ) { }

  resolveZip (value: string): Observable<City> {
    return this.http.get<City>(`http://localhost:3001/zip/${ value }`);
  }

  search (personalData: PersonalData): Observable<Product[]> {
    return this.http
      .get<Product[]>(`http://localhost:3001/products/${ personalData.zipCode }/${ personalData.dateOfBirth }`);
  }
}
