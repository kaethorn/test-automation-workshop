import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ProductFormComponent } from './product-form.component';
import { ProductDataService } from './../product-data.service';
import { Product } from './../product';

describe('ProductFormComponent', () => {
  let component: ProductFormComponent;
  let fixture: ComponentFixture<ProductFormComponent>;
  let productDataService: jasmine.SpyObj<ProductDataService>;

  beforeEach(async(() => {
    productDataService = jasmine.createSpyObj('ProductDataService', [ 'resolveZip', 'search' ]);
    TestBed.configureTestingModule({
      imports     : [ FormsModule, HttpClientModule ],
      declarations: [ ProductFormComponent ],
      providers   : [{ provide: ProductDataService, useValue: productDataService }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFormComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should initialize products`, async(() => {
    expect(component.products).toEqual([]);
  }));

  it('should render title in a h1 tag', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Product Finder ™');
  }));

  describe('#setZip', () => {

    describe('on success', () => {

      beforeEach(() => {
        productDataService.resolveZip.and
          .returnValue(of({ name: 'Zürich', zipCode: '8000' }));
      });

      it('sets the city name', () => {
        component.setZip('8000');
        expect(productDataService.resolveZip.calls.mostRecent().args).toEqual(['8000']);
        expect(component.model.cityName).toEqual('Zürich');
      });
    });

    describe('on error', () => {

      beforeEach(() => {
        productDataService.resolveZip.and.returnValue(of({}));
      });

      it('sets an error message', () => {
        component.setZip('0000');
        expect(productDataService.resolveZip.calls.mostRecent().args).toEqual(['0000']);
        expect(component.model.cityName).toEqual('Could not look up zip code.');
      });
    });
  });

  describe('#onSubmit', () => {

    beforeEach(() => {
      const products: Product[] = new Array<Product>();

      products.push({ category: '2', name: 'Green' });
      products.push({ category: '1', name: 'Red' });
      products.push({ category: '3', name: 'Blue' });

      productDataService.search.and.returnValue(of(products));
      component.model.cityName = 'Zürich';
      component.model.zipCode  = '8000';
      component.model.dateOfBirth = '07.03.1980';
    });

    it('populates products', () => {
      expect(component.products.length).toBe(0);

      component.onSubmit();

      expect(productDataService.search.calls.mostRecent().args).toEqual([ component.model ]);
      expect(component.products.length).toBe(3);
    });

    it('sorts products by category', () => {
      component.onSubmit();
      expect(component.products[0]).toEqual({ category: '1', name: 'Red' });
      expect(component.products[1]).toEqual({ category: '2', name: 'Green' });
      expect(component.products[2]).toEqual({ category: '3', name: 'Blue' });
    });
  });
});
