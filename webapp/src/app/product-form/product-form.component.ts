import { Component, OnInit } from '@angular/core';
import { PersonalData } from '../personal-data';
import { ProductDataService } from '../product-data.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})

export class ProductFormComponent implements OnInit {
  constructor(private productDataService: ProductDataService) { }

  model = new PersonalData();

  products = [];

  ngOnInit() {
    this.model.cityName = '';
  }

  setZip(value: string): void {
    this.productDataService.resolveZip(value)
    .subscribe(city => {
      if (city.name) {
        this.model.cityName = city.name;
      } else if (value.length === 4) {
        this.model.cityName = 'Could not look up zip code.';
      }
    });
  }

  onSubmit () {
    this.productDataService.search(this.model)
    .subscribe(products => {
      this.products = products.sort((productA, productB) => {
        return (productA.category < productB.category) ? -1 : 1;
      });
    });
  }
}
