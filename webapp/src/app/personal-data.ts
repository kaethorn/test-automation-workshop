export class PersonalData {
  name: string;
  dateOfBirth: string;
  zipCode: string;
  cityName: string;
}
