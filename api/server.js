const express  = require('express');
const app      = express();

const ZipCodes = new (require('../backend/zip_codes'))();
const Products = new (require('../backend/products'))();

const CorsHeaders = {
  'Access-Control-Request-Headers': 'content-type',
  'Access-Control-Allow-Origin'   : '*'
};

const silent = process.argv.includes('--silent');

app.listen(3001, () => {
  if (process.send) {
    process.send('Server started');
  }
});

app.get('/zip/:zip', (req, res) => {
  const input = Number.parseInt(req.params.zip);
  if (!silent) {
    // eslint-disable-next-line no-console
    console.log(`zip code lookup for ${ input }.`);
  }
  res.set(CorsHeaders);
  res.send({
    name   : ZipCodes.lookup(input),
    zipCode: input
  });
});

app.get('/products/:zip/:dob', (req, res) => {
  if (!silent) {
    // eslint-disable-next-line no-console
    console.log(`product lookup for ${ req.params.zip } and ${ req.params.dob }.`);
  }
  res.set(CorsHeaders);
  res.send(Products.match(req.params.zip, req.params.dob));
});
