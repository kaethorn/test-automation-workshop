const { fork } = require('child_process');
const http = require('http');

describe('server', () => {

  beforeEach(function (done) {
    this.server = fork('server', [ '--silent' ]);
    this.server.on('message', data => {
      if (data === 'Server started') {
        done();
      }
    });
  });

  afterEach(function () {
    this.server.kill();
  });

  it('listens on port 3001', done => {
    const net = require('net');
    const client = net.createConnection({ port: 3001 }, () => {
      expect(client.remotePort).toBe(3001);
      done();
    });
  });

  describe('/zip/:zip', () => {

    beforeEach(function (done) {
      http.get({
        hostname: 'localhost',
        port    : 3001,
        path    : '/zip/8000'
      }, (res) => {
        let rawData = '';
        res.on('data', (chunk) => {
          rawData += chunk;
        });
        res.on('end', () => {
          try {
            this.response = JSON.parse(rawData);
            done();
          } catch (e) {}
        });
      });
    });

    it('resolves zip codes to city names', function () {
      expect(this.response.name).toEqual('Zürich');
    });
  });

  describe('/products/:zip/:dob', () => {

    beforeEach(function (done) {
      http.get({
        hostname: 'localhost',
        port    : 3001,
        path    : '/products/8000/10.04.1999'
      }, (res) => {
        let rawData = '';
        res.on('data', (chunk) => {
          rawData += chunk;
        });
        res.on('end', () => {
          try {
            this.response = JSON.parse(rawData);
            done();
          } catch (e) {}
        });
      });
    });

    it('returns matching products', function () {
      expect(this.response.length).toBe(6);
    });
  });
});
